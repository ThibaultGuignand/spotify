<?php

namespace App\Models;

require __DIR__ . '/../../vendor/autoload.php';


class ArtistsModel
{
   public function insertArtists($database, $artists) {
        foreach ($artists as $artist) {
            list($name, $description) = $artist;
            $database->query("INSERT INTO artists (name, description) VALUES (:name, :description)", [$name, $description]);
        }
    }
}