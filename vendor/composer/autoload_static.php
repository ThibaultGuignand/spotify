<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit2ecfe7a5a93ca20908ba4bdffa69eb9d
{
    public static $files = array (
        'cfe4039aa2a78ca88e07dadb7b1c6126' => __DIR__ . '/../..' . '/config.php',
    );

    public static $prefixLengthsPsr4 = array (
        'D' => 
        array (
            'Databases\\' => 10,
        ),
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Databases\\' => 
        array (
            0 => __DIR__ . '/../..' . '/databases',
        ),
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/app',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit2ecfe7a5a93ca20908ba4bdffa69eb9d::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit2ecfe7a5a93ca20908ba4bdffa69eb9d::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit2ecfe7a5a93ca20908ba4bdffa69eb9d::$classMap;

        }, null, ClassLoader::class);
    }
}
