<?php
session_start();

require_once '../vendor/autoload.php';

switch ($_SERVER['REQUEST_URI']) {
    case '/':
        require_once '../resources/views/index.html';
        break;
    case '/editorials':
        require_once '../resources/views/editorials.html';
        break;
    case '/tracklist':
        require_once '../resources/views/tracklist.html';
        break;
}
