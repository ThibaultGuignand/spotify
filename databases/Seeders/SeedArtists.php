<?php

namespace Databases\Seeders;

use Databases\Connection;

final class SeedArtists extends Connection {
    public function handle() {
        $queries = [
            "INSERT INTO artists (id, name,description) VALUES (1, 'SCH', 'SCH, de son vrai nom Julien Schwarzer, est un 
rappeur français, né le 6 avril 1993 à Marseille. Son identité musicale se caractérise par une voix grave et calme,
l\'utilisation de thèmes tels que la mort, l\'amour ou la richesse, des références aux films d\'horreur et de gangsters 
ainsi que des instrumentaux trap ou cloud rap.')",
            "INSERT INTO artists (id, name,description) VALUES (2, 'DAMSO', 'Damso est un rappeur belge originaire de Kinshasa.
Il c’est lancé en solo en 2013. Sa musique est souvent considérée comme introspective et mélancolique, et ses textes 
abordent des thèmes tels que l\'identité et les relations amoureuses. Il a sorti plusieurs albums, dont Ipséité en 2017
et Lithopédion en 2018, qui ont tous les deuxété très bien accueillis par la critique et parles fans.')",
            "INSERT INTO artists (id, name,description) VALUES (3, 'ANGELE', 'Angèle, née le 3 décembre 1995 à Uccle
(Bruxelles-Capitale), est une auteure-compositrice-interprète, musicienne, productrice, actrice et mannequin belge.
Son premier album, Brol, sorti en octobre 2018 et certifié double disque de diamant. En 2021 sort son album
Nonante-Cinq, qui connaît un énorme succès. En 2020, elle remporte la Victoire de la musique du concert de l\'année 
pour son Brol Tour.')",
            "INSERT INTO artists (id, name,description) VALUES (4, 'LOMEPAL', 'Lomepal est un chanteur français originaire 
de Paris. Il a fait ses débuts dans la musique en 2015 avec son EP Flip. Depuis, il a sorti plusieurs albums, dont 
Jeannine, ou Mauvais Ordre en 2022 toujours très bien accueillis par la critique et par les fans. Ses chansons arborent 
souvent des thèmes tels que l\'amour, la mort, la perte et la quête de soi.')"
        ];

        $pdo = $this->connection;
        $stmt = $pdo->prepare($queries[0]);
        $stmt->execute();
        $stmt = $pdo->prepare($queries[1]);
        $stmt->execute();
        $stmt = $pdo->prepare($queries[2]);
        $stmt->execute();
        $stmt = $pdo->prepare($queries[3]);
        $stmt->execute();
    }

}