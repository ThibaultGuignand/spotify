import {defineConfig} from "vite";

export default defineConfig({
    build: {
        rollupOptions: {
            // overwrite default .html entry
            input: '/resources/scripts/index.js',
        },
    },
})