import '../styles/styles.scss'
import homepage from './pages/homepage.js';
import editorials from './pages/editorials.js';
import cursor from './components/cursor.js';
import homeToDescription from "./transitions/home-transition.js";
cursor();


const HOMEPAGE = document.getElementById("homepage");
const EDITORIALS = document.getElementById("editorials");

if (HOMEPAGE) {
    homepage();
    homeToDescription();
    document.getElementById('explore').addEventListener('click', async () => {
        document.getElementById('description').innerHTML = (await import("./pages/description.js")).default();
    }
    );

} else if (EDITORIALS) {
    editorials();
}




