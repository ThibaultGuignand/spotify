import {factor as FACTOR_CONFIG} from "../config.js";

export default () => {
    const covers = document.querySelectorAll('[id^="cover"]');

    window.addEventListener('scroll', () => {
        let scrollTop = window.scrollY;

        covers.forEach((cover, i) => {
            let angle = -7 * (i - 1); // initial incline
            angle += (i % 2 === 0 ? 1 : -1) * scrollTop * FACTOR_CONFIG.rotation * (i + 3);
            let translation = (i % 2 === 0 ? 1 : -1) * scrollTop * FACTOR_CONFIG.translation * (i * .3);
            let offset = (i % 2 === 0 ? 1 : -1) * scrollTop * FACTOR_CONFIG.offset * (i + .3);

            cover.style.transform = `translate(${offset}px, ${translation}px) rotate(${angle}deg)`;
        });
    });
}