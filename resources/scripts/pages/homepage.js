import gsap from 'gsap';

import ExploreNowImage from '../assets/images/explore-now.png';
import {cover} from '../config.js';

export default () => {
    const COLS_NUMBER = 3;
    const MAIN_ELEMENT = document.getElementById('main');
    const CIRCLE_ELEMENT = document.getElementById('circle');
    const NEXT_ELEMENT = document.getElementById('next');
    const PREV_ELEMENT = document.getElementById('prev');
    const ANIMATE_OPTIONS = {
        duration: 2.3,
        ease: "power4.inOut"
    };

    let cols = [];
    let index = 0;
    let in_progress = false;
    let scrollTimeout;

    let generateCore = () => {
        const CORE_ELEMENT = document.createElement("div");
        CORE_ELEMENT.classList.add("core");
        CORE_ELEMENT.innerHTML = `
<div class="wrap">
    <ul class="transition-in">
        <li class="deseaper">SCH</li>
        <li class="deseaper"><a id="explore" href="#"><img src="${ExploreNowImage}" alt="explore"></a></li>
        <li class="appear"><a id="back-to-slider" href="#">Back to slider</a></li>
    </ul>
</div>
    `;

        return CORE_ELEMENT;
    }
    let createCols = () => {
        for (let col = 0; col < COLS_NUMBER; col++) {
            const COL_ELEMENT = document.createElement('div');
            const SECTION_ELEMENT = document.createElement('div');
            const IMAGE_ELEMENT = document.createElement('img');

            COL_ELEMENT.style.setProperty('--x', -100 / COLS_NUMBER * col + 'vw');
            if ((col + 1) % 2) {
                COL_ELEMENT.className = 'col odd transition-out';
            } else {
                COL_ELEMENT.className = 'col pair transition-in';
                MAIN_ELEMENT.appendChild(
                    generateCore()
                );
            }

            SECTION_ELEMENT.className = "section";

            IMAGE_ELEMENT.src = cover[index];

            SECTION_ELEMENT.appendChild(IMAGE_ELEMENT);
            COL_ELEMENT.appendChild(SECTION_ELEMENT);
            MAIN_ELEMENT.appendChild(COL_ELEMENT);

            cols.push(COL_ELEMENT);
        }
    }
    let wheel = (e) => {
        clearTimeout(scrollTimeout);
        setTimeout(function () {
            if (e.deltaY < -40) {
                go(-1);
            } else if (e.deltaY >= 40) {
                go(1);
            }
        })
    }
    let go = dir => {
        if (in_progress) {
            return;
        }

        in_progress = true;

        if (index + dir < 0) index = cover.length - 1;
        else if (index + dir >= cover.length) index = 0;
        else index += dir;

        let turnCircle = (clockwise = true) => {
            let rotateCircleVariableCSS = getComputedStyle(CIRCLE_ELEMENT).getPropertyValue('--rotate-circle');
            return parseInt(rotateCircleVariableCSS.split("deg")[0]) + (clockwise ? 80 : -80);
        }

        function up(col, next) {
            col.appendChild(next);
            let x = turnCircle();

            gsap.to(CIRCLE_ELEMENT, {duration: 2.5, "--rotate-circle": `${x}deg`, ease: "power4.inOut"});
            gsap.to(col, {...ANIMATE_OPTIONS, y: -window.innerHeight}).then(() => {
                col.children[0].remove();
                gsap.to(col, {duration: 0, y: 0});
            })
        }

        function down(col, next) {
            col.prepend(next);
            let x = turnCircle(false);

            gsap.to(CIRCLE_ELEMENT, {duration: 2.5, "--rotate-circle": `${x}deg`, ease: "power4.inOut"});
            gsap.to(col, {duration: 0, y: -window.innerHeight});
            gsap.to(col, {...ANIMATE_OPTIONS, y: 0}).then(() => {
                col.children[1].remove();
                in_progress = false;
            })
        }

        for (let i in cols) {
            let col_element = cols[i];
            const NEXT_ELEMENT = document.createElement('div');
            const IMAGE_ELEMENT = document.createElement('img');

            NEXT_ELEMENT.className = 'section';

            IMAGE_ELEMENT.src = cover[index];

            NEXT_ELEMENT.appendChild(IMAGE_ELEMENT);

            if ((i - Math.max(0, dir)) % 2) {
                down(col_element, NEXT_ELEMENT);
            } else {
                up(col_element, NEXT_ELEMENT);
            }
        }
    }

    window.addEventListener('keydown', function (e) {
        if (['ArrowDown', 'ArrowRight'].includes(e.key)) {
            go(1);
        } else if (['ArrowUp', 'ArrowLeft'].includes(e.key)) {
            go(-1);
        }
    });
    window.addEventListener('mousewheel', wheel, false);
    window.addEventListener('wheel', wheel, false);
    NEXT_ELEMENT.addEventListener('click', () => {
        go(-1);
    });
    PREV_ELEMENT.addEventListener('click', () => {
            go(1);
        }
    );

    createCols();
}