export default () => {
    return `
<main>
        <h1>SCH</h1>
    </main>
    <footer>
        <ul>
            <li>SCH, de son vrai nom Julien Schwarzer, est un rappeur français, né le 6 <br> avril 1993 à Marseille.
                Son identité musicale se caractérise par une voix <br> grave et calme, l'utilisation de thèmes tels
                que la
                mort, l'amour ou la <br> richesse, des références aux films d'horreur et de gangsters ainsi
                que<br> des
                instrumentaux trap ou cloud rap.
            </li>
            <li><a href="../../views/editorials.html">Editorials</a></li>
            <li>New arrivals</li>
        </ul>
    </footer>
`
}
