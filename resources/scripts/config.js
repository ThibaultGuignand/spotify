export const cover = [
    "/public/images/sch/sch-full.png",
    "/public//images/damso/damso-full.png",
    "/public/images/angele/angele-full.png",
    "/public/images/lomepal/lomepal-full.png"
];
export const cursor = {
    'size': 8,
    'size_focus': 36,
    'follow_speed': .16,
}
export const factor = {
    rotation: 5 / 600,
    translation: 45 / 600,
    offset: 40 / 600,
}