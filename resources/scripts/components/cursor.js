import gsap from "gsap";
import {cursor as CURSOR_CONFIG} from "../config.js";

export default () => {

    let cursor_position = {
        x: {start: 0, end: 0},
        y: {start: 0, end: 0},
    }

    let lerp = (start, end, amount) => {
        return (1 - amount) * start + amount * end
    }
    let createCursor = () => {
        const CURSOR_ELEMENT = document.createElement('div');
        const CURSOR_F_ELEMENT = document.createElement('div');

        CURSOR_ELEMENT.className = 'cursor';
        CURSOR_F_ELEMENT.className = 'cursor-f';

        document.querySelector('.wrapper').appendChild(CURSOR_ELEMENT);
        document.querySelector('.wrapper').appendChild(CURSOR_F_ELEMENT);

        CURSOR_ELEMENT.style.setProperty('--size', CURSOR_CONFIG.size + 'px');
        CURSOR_F_ELEMENT.style.setProperty('--size', CURSOR_CONFIG.size_focus + 'px');

        return {'cursor': CURSOR_ELEMENT, 'cursor_f': CURSOR_F_ELEMENT};
    }
    let followCursor = () => {
        cursor_position.x.start = lerp(cursor_position.x.start, cursor_position.x.end, CURSOR_CONFIG.follow_speed);
        cursor_position.y.start = lerp(cursor_position.y.start, cursor_position.y.end, CURSOR_CONFIG.follow_speed);

        cursors.cursor_f.style.top = cursor_position.y.start - CURSOR_CONFIG.size_focus / 2 + 'px';
        cursors.cursor_f.style.left = cursor_position.x.start - CURSOR_CONFIG.size_focus / 2 + 'px';

        requestAnimationFrame(followCursor);
    }
    let mousedown = e => {
        gsap.to(cursors.cursor, {scale: 4.5});
        gsap.to(cursors.cursor_f, {scale: .4});
    }
    let mouseup = e => {
        gsap.to(cursors.cursor, {scale: 1});
        gsap.to(cursors.cursor_f, {scale: 1});
    }

    window.addEventListener('mousemove', function (e) {
        cursor_position.x.end = e.clientX;
        cursor_position.y.end = e.clientY;
        cursors.cursor.style.left = e.clientX - CURSOR_CONFIG.size / 2 + 'px';
        cursors.cursor.style.top = e.clientY - CURSOR_CONFIG.size / 2 + 'px';
    });
    window.addEventListener('mousedown', mousedown, false);
    window.addEventListener('touchstart', mousedown, false);
    window.addEventListener('touchend', mouseup, false);
    window.addEventListener('mouseup', mouseup, false);

    let cursors = createCursor();
    followCursor();

    if ('ontouchstart' in window) {
        cursors.cursor.style.display = 'none';
        cursors.cursor_f.style.display = 'none';
    }
}