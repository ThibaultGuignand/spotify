import {gsap} from "gsap";

export default () => {
    const PAIRCOLS = document.querySelectorAll('.transition-in');
    const CIRCLE = document.getElementById('circle');
    const EXPLORE = document.getElementById('explore');
    const ELEMENTS = document.querySelectorAll('.transition-out, .next, .left-line, .right-line, .prev');
    const BORDER = document.querySelectorAll('.home-list');
    const BACKTOSLIDER = document.querySelectorAll('.appear');
    const EXPLORENOW = document.querySelectorAll('.deseaper');
    const BACK = document.getElementById('back-to-slider');

    const ANIMATION = gsap.timeline();


    ANIMATION
        .to(PAIRCOLS, {duration: 1.5, x: '-100%', zIndex: 1, ease: "power4.inOut",})
        .to(CIRCLE, {duration: 1.5, x: '-100%', delay: -1.5, ease: "power4.inOut", rotate: 90})
        .to(ELEMENTS, {duration: 0.5, opacity: 0, delay: -1.5, width: '0vw'})
        .to(BORDER, {borderBottom: 'none', borderTop: 'none', delay: -1.5})
        .to(BACKTOSLIDER, { opacity:1, display: 'block', delay: -1, duration: 0,})
        .to(EXPLORENOW, { opacity: 0,display: 'none',  delay: -1, duration: 0, });

    ANIMATION.eventCallback("onComplete", () => {
        document.querySelector('.description').style.zIndex = 999;
    }
    );

    ANIMATION.pause();
    EXPLORE.addEventListener('click', async () => {
        document.querySelector('.description').style.display = 'block';
        document.querySelector('.pair').style.zIndex = 999;
        ANIMATION.play();

    });

    BACK.addEventListener('click', async () => {
        document.querySelector('.description').style.zIndex = -999;
        await ANIMATION.reverse();
        if(document.querySelector('.description')) {
            document.querySelector('.description').style.display = 'none';
        }

    });

}